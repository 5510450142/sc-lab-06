package item2;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Grizzly_Cat grizzy = new Grizzly_Cat("Aerng");
		Panda  panda = new Panda("Ploy");
		Zebra zebra = new Zebra("Pa");
		
		Eating_Cat eating = new Eating_Cat();
		System.out.println(eating.makeBearEat(grizzy));
		System.out.println(eating.makeBearEat(panda));
		System.out.println(eating.makeBearEat(zebra));
		System.out.println(eating.makeBearEat((Cat) zebra));
	}

}
