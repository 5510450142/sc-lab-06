package item2;

public abstract class Cat {
	private String name;
	
	public Cat(String name){
		this.name = name;
	}
	
	public abstract String eat();
	
	public String toString(){
		return this.name;
	}
}
