package item2;

public class Eating_Cat {
	public String makeBearEat(Panda panda){
		return "Panda: "+panda.eat();
	}
	public String makeBearEat(Zebra rabbit){
		return "Rabbit: "+rabbit.eat();
	}
	public String makeBearEat(Grizzly_Cat grizzy){
		return "Grizzy: "+grizzy.eat();
	}
	public String makeBearEat(Cat lion){
		return "Cat: "+lion.eat();
	}
}
